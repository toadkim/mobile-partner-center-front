import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {PageNotFoundComponent} from './pages/error/page-not-found/page-not-found.component';
import {JoinComponent} from './pages/join/join.component';


/*
  전체 레벨 라우팅 정의 파일
 */


const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'join', component: JoinComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),

  ],
  exports: [
    RouterModule
  ]
})
export class AppRouteModule {
}
