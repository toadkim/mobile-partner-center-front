import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomFileUploadComponent, ImageCropPopupComponent} from './common-file-upload/custom-file-upload.component';
import {ShareModule} from '../modules/share.module';
import {ReactiveFormsModule} from '@angular/forms';
import {
  CommonBottomSheetPopupComponent,
  CommonBottomSheetPopupSheetComponent
} from './common-bottom-sheet-popup/common-bottom-sheet-popup.component';


@NgModule({
  declarations: [CustomFileUploadComponent, CommonBottomSheetPopupComponent, CommonBottomSheetPopupSheetComponent, ImageCropPopupComponent],
  imports: [
    CommonModule,
    ShareModule,
    ReactiveFormsModule
  ],
  exports: [
    CustomFileUploadComponent,
    CommonBottomSheetPopupComponent
  ]
})
export class CommonComponentsModule {
}
