import {Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ImageCroppedEvent, ImageCropperComponent, LoadedImage} from 'ngx-image-cropper';


@Component({
  selector: 'app-custom-file-upload',
  templateUrl: './custom-file-upload.component.html',
  styleUrls: ['./custom-file-upload.component.scss']
})
export class CustomFileUploadComponent implements OnInit {
  imagePath: any;
  imgUrl: any;
  formControl: FormControl;
  @Input() label: string[];
  @Input() customClass?: string;
  @Input() outputWidth: number;
  @Input() outputHeight: number;
  @Output() formChanged = new EventEmitter<FormControl>();

  constructor(public dialog: MatDialog) {
    this.formControl = new FormControl('');
    this.label = [''];
    this.outputWidth = 745;
    this.outputHeight = 1080;
  }

  ngOnInit(): void {
  }

  preview(files: FileList | null) {
    if (files) {
      if (files.length === 0)
        return;

      var mimeType = files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }

      var reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        const dialogRef = this.dialog.open(ImageCropPopupComponent, {
          data: {
            cropTargetImage: reader.result,
            outputImageFileWidth: this.outputWidth,
            outputImageFileHeight: this.outputHeight
          },
          width: '100vw',
          maxWidth: '600px'
        });
        dialogRef.afterClosed().subscribe(result => {
          this.imgUrl = result;
          this.formChanged.emit(this.formControl);
        })
      }
    }
  }

  clear() {
    this.imgUrl = '';
  }


  get formLabel(): string {
    return this.label.join('<br/>');
  }
}


@Component({
  template: `
    <div style="overflow: hidden;">
      <mat-dialog-content>
        <mat-spinner *ngIf="!this.isLoadComplete" style="margin: 10px auto;"></mat-spinner>
        <image-cropper
          [imageBase64]="this.cropTargetImage"
          [imageChangedEvent]="imageChangedEvent"
          [maintainAspectRatio]="true"
          [aspectRatio]="(this.outputWidth / this.outputHeight)"
          [resizeToWidth]="this.outputWidth"
          [resizeToHeight]="this.outputHeight"
          format="png"
          (imageCropped)="imageCropped($event)"
          (imageLoaded)="imageLoaded($event)"
          (cropperReady)="cropperReady()"
          (loadImageFailed)="loadImageFailed()"
          [autoCrop]="false"
        ></image-cropper>
      </mat-dialog-content>
      <button class="round" mat-flat-button color="accent" fxFlexFill (click)="crop()">등록하기</button>
    </div>`
})
export class ImageCropPopupComponent implements OnInit {
  @Input() cropTargetImage: any;
  @Input() outputWidth: number;
  @Input() outputHeight: number;

  isLoadComplete: boolean;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  @ViewChild(ImageCropperComponent) imageCropper!: ImageCropperComponent;

  constructor(@Inject(MAT_DIALOG_DATA) data: { cropTargetImage: string, outputImageFileWidth: number, outputImageFileHeight: number }, public dialogRef: MatDialogRef<ImageCropPopupComponent>) {
    this.cropTargetImage = data.cropTargetImage;
    this.isLoadComplete = false;
    this.outputWidth = data.outputImageFileWidth;
    this.outputHeight = data.outputImageFileHeight;
  }


  crop() {
    this.imageCropper.crop();
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.dialogRef.close(this.croppedImage);
  }

  imageLoaded(image: LoadedImage) {
    // show cropper
    this.isLoadComplete = true;
    console.log(image);

  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }

  ngOnInit(): void {
  }
}
