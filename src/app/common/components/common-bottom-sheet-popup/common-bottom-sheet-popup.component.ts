import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-common-bottom-sheet-popup',
  templateUrl: './common-bottom-sheet-popup.component.html',
  styleUrls: ['./common-bottom-sheet-popup.component.scss']
})
export class CommonBottomSheetPopupComponent implements OnInit {

  btSheet: MatBottomSheet;
  constructor(private bottomSheet: MatBottomSheet) {
    this.btSheet = bottomSheet;
  }

  ngOnInit(): void {
    //this.bottomSheet.open(CommonBottomSheetPopupSheetComponent);
    this.open();
  }

  open(): void {
    this.btSheet.open(CommonBottomSheetPopupSheetComponent, {data: {innerHtml: '<div>aaaa</div>'}});
  }


}


@Component({
  selector: 'common-bottom-sheet',
  template: '<div><div [innerHTML]="this.sheetHtml"></div><button mat-flat-button color="primary" (click)="buttonClick($event);">닫기</button></div>'
})
export class CommonBottomSheetPopupSheetComponent {
  sheetHtml:string;

  constructor(private bottomSheetRef: MatBottomSheetRef, @Inject(MAT_BOTTOM_SHEET_DATA) public data: {innerHtml: string}) {
    this.sheetHtml = this.data.innerHtml;
  }
  buttonClick(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
