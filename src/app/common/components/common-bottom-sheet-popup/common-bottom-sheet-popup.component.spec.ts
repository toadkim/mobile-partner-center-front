import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonBottomSheetPopupComponent } from './common-bottom-sheet-popup.component';

describe('CommonBottomSheetPopupComponent', () => {
  let component: CommonBottomSheetPopupComponent;
  let fixture: ComponentFixture<CommonBottomSheetPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonBottomSheetPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonBottomSheetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
