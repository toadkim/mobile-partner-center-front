export enum Enums {
  NoHeaderAndNoFooter,
  NoHeader,
  NoFooter,
  All
}


export enum HeaderType {
  OnlyArrow,
  OnlyClose,
  All,
  None
}


export enum TosTemplateClassName {
  ProductExposure = './content/product-exposure.html',
  ProductSalesApproval = './content/product-sales-approval.html',
  ConsumerProtection = './content/consumer-protection.html',
  AgeCheck = './content/age-check.html'
}
