import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ImageCropperModule} from 'ngx-image-cropper';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FlexLayoutModule,
    /* Custom Module */
    MaterialModule,
    ImageCropperModule
  ],
  exports: [
    /* Angular Module */
    FlexLayoutModule,
    /* Custom Module */
    MaterialModule,
    ImageCropperModule
  ]
})
export class ShareModule {
}
