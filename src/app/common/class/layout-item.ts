export class LayoutItem {
  visible: boolean;
  label: string;

  constructor() {
    this.visible = true;
    this.label = '';
  }

  create(label?: string, visible?: boolean): LayoutItem {
    this.label = label === undefined ? '' : label;
    this.visible = visible === undefined ? true : visible;
    return this;
  }
}


export class IconMenuItem extends LayoutItem {
  icon: string;

  constructor() {
    super();
    this.icon = '';
  }

  create(label?: string, visible?: boolean, icon?: string): IconMenuItem {
    if (icon === undefined) {
      return super.create(label, visible) as IconMenuItem;
    } else {
      this.label = label === undefined ? '' : label;
      this.visible = visible === undefined ? true : visible;
      this.icon = icon === undefined ? '' : icon;
      return this;
    }
  }
}

export class FooterMenuItem extends IconMenuItem {
  routeLink: string;

  constructor() {
    super();
    this.routeLink = '';
  }

  create(label?: string, visible?: boolean, icon?: string, routeLink?: string): FooterMenuItem {
    if (routeLink === undefined) {
      return super.create(label, visible, icon) as FooterMenuItem;
    } else {
      this.label = label === undefined ? '' : label;
      this.visible = visible === undefined ? true : visible;
      this.icon = icon === undefined ? '' : icon;
      this.routeLink = routeLink === undefined ? '' : routeLink;
      return this;
    }

  }
}
