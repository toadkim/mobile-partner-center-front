export class User {
  private id: string;
  isLoggedIn: boolean;

  constructor() {
    this.id = '';
    this.isLoggedIn = false;
  }
}
