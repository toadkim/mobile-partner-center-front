export class ApiResponse {
  code:number;
  msg: string;
  data: any;

  constructor(code:number, msg: string, data: any) {
    this.code = code;
    this.msg = msg;
    this.data = data;
  }
}
