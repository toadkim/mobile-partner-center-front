import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class CustomValidators {

  static passwordConfirmValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const password = control.parent?.get('password');
    const passwordConfirm = control.parent?.get('passwordConfirm');

    return password && passwordConfirm && password.value !== passwordConfirm.value ? { passwordNotMatch : true } : null;
  };

}
