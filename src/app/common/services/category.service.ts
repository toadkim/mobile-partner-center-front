import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ApiResponse} from '../class/api-response';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private apiBasicPath = environment.apiPath + 'category/';
  constructor(private http: HttpClient) {
    console.log(environment.apiPath);
  }

  all() {
    return this.http.get<ApiResponse>(this.apiBasicPath+'all').pipe();
  }

}
