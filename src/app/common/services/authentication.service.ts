import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../class/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userBehaviorSubject: BehaviorSubject<User>;
  public currentUser$: Observable<User>;

  constructor() {
    const storedUserInfo = localStorage.getItem('currentUser$') as string;
    this.userBehaviorSubject = new BehaviorSubject<User>(JSON.parse(storedUserInfo));
    this.currentUser$ = this.userBehaviorSubject.asObservable();
  }

  public get currentUserValue():User{
    return this.userBehaviorSubject.value;
  }

  signIn(user:User) {
    localStorage.setItem('currentUser$', JSON.stringify(user));
    this.userBehaviorSubject.next(user);
  }

}
