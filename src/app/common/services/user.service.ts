import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ApiResponse} from '../class/api-response';
import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {Observable, of, timer} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiBasicPath = environment.apiPath;

  constructor(private http: HttpClient) {
    console.log(environment.apiPath);
  }

  convertFromZeliter(token: string) {
    return this.http.get<ApiResponse>(this.apiBasicPath + 'convert/user', {headers: {'X-08liter-Token': token}}).pipe();
  }

  checkEmail(email: string) {
    return this.http.get<ApiResponse>(this.apiBasicPath + 'check/email', {params: {email: email}}).pipe();
  }

  checkBusiness(businessNumber: string) {
    return this.http.get<ApiResponse>(this.apiBasicPath + 'check/business', {params: {number: businessNumber}}).pipe();
  }

  join(joinData:any) {
    return this.http.put<ApiResponse>(this.apiBasicPath+'login/register', {params: joinData}).pipe();
  }

}

export function UniqueEmailAsyncValidator(userService: UserService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return timer(500).pipe(
      map(() => control.value),
      switchMap((email) => userService.checkEmail(email)),
      map((response: ApiResponse) => response.code === 200 ? null : {existEmail: true}),
      catchError(() => of(null))
    )
  };
}


export function UniqueBusinessAsyncValidator(userService: UserService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return timer(500).pipe(
      map(() => control.value),
      switchMap((busnessNumber) => userService.checkBusiness(busnessNumber)),
      map((response: ApiResponse) => response.code === 200 ? null : {existBusinessNumber: true}),
      catchError(() => of(null))
    )
  };
}
