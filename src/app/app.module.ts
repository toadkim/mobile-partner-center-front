import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRouteModule} from './app-route.module';
import {LayoutProviderModule} from './layout/layout-provider.module';
import {PageModule} from './pages/page.module';
import {ShareModule} from './common/modules/share.module';
import {CommonComponentsModule} from './common/components/common-components.module';
import {BrowserModule, HammerModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HammerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    PageModule,
    LayoutProviderModule,
    ShareModule,
    CommonComponentsModule,
    AppRouteModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
