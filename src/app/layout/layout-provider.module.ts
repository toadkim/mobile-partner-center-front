import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MenuItemComponent} from './footer/menu-item/menu-item.component';
import {AppRouteModule} from '../app-route.module';
import {LayoutProviderService} from './layout-provider.service';
import {ShareModule} from '../common/modules/share.module';



@NgModule({
  declarations: [HeaderComponent, FooterComponent, MenuItemComponent],
  imports: [
    CommonModule,
    ShareModule,
    AppRouteModule
  ],
  exports: [HeaderComponent, FooterComponent, MenuItemComponent],
  providers: [LayoutProviderService]
})
export class LayoutProviderModule {
}
