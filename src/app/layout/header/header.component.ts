import {Component, OnDestroy, OnInit, Output} from '@angular/core';
import {LayoutProviderService} from '../layout-provider.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router'
import {IconMenuItem} from '../../common/class/layout-item';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  visible: boolean;
  arrow: IconMenuItem;
  closeBtn: IconMenuItem;
  title: string;

  constructor(private layoutService: LayoutProviderService, private location: Location, private router: Router) {
    this.visible = this.layoutService.getHeaders().visible;
    this.arrow = this.layoutService.getHeaders().arrow;
    this.closeBtn = this.layoutService.getHeaders().closeBtn;
    this.title = this.layoutService.getHeaders().title;
    this.layoutService.layoutHeaderSubject.subscribe((header)=>{
      this.visible = header.visible;
      this.arrow = header.arrow;
      this.closeBtn = header.closeBtn;
      this.title = header.title;
    })
  }

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }

  close(): void {
    this.router.navigateByUrl('/').then();
  }

  ngOnDestroy():void {
    this.layoutService.layoutHeaderSubject.unsubscribe();
  }

}
