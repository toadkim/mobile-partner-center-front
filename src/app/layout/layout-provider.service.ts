import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {FooterMenuItem, IconMenuItem} from '../common/class/layout-item';
import {HeaderType, Enums} from '../common/enum/enums';


@Injectable()
export class LayoutProviderService {

  private headers: any = {
    visible: true,
    arrow: new IconMenuItem().create('', true, 'keyboard_backspace'),
    title: '',
    closeBtn: new IconMenuItem().create('', true, 'close')
  };

  private footers: any = {
    visible: true,
    menus: [
      new FooterMenuItem().create('홈', true, 'home', '/a'),
      new FooterMenuItem().create('홈2', true, 'alarm', '/join'),
      new FooterMenuItem().create('홈3', true, 'credit_card', '/campaign'),
      new FooterMenuItem().create('홈4', true, 'favorite', '/login')
    ]
  };

  public layoutHeaderSubject = new Subject<any>();
  public layoutFooterSubject = new Subject<any>();


  updateLayoutByPageType(headerType: HeaderType, pageType: Enums, title?: string) {

    switch (pageType) {
      case Enums.All:
        this.headers.visible = true;
        this.footers.visible = true;
        break;
      case Enums.NoHeaderAndNoFooter:
        this.headers.visible = false;
        this.footers.visible = false;
        break;
      case Enums.NoHeader:
        this.headers.visible = false;
        this.footers.visible = true;
        break;
      case Enums.NoFooter:
        this.headers.visible = true;
        this.footers.visible = false;
        break;
    }

    if(this.headers.visible) {
      switch (headerType) {
        case HeaderType.All:
          this.headers.arrow.visible = true;
          this.headers.closeBtn.visible = true;
          break;
        case HeaderType.OnlyArrow:
          this.headers.arrow.visible = true;
          this.headers.closeBtn.visible = false;
          break;
        case HeaderType.OnlyClose:
          this.headers.arrow.visible = false;
          this.headers.closeBtn.visible = true;
          break;
        case HeaderType.None:
          this.headers.arrow.visible = false;
          this.headers.closeBtn.visible = false;
          break;

      }
      this.headers.title = title === undefined ? '' : title;
    }

    this.layoutHeaderSubject.next(this.headers);
    this.layoutFooterSubject.next(this.footers);
  }

  getHeaders(): any {
    return this.headers;
  }

  getFooters(): any {
    return this.footers;
  }
}
