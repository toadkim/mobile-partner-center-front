import {Component, OnDestroy, OnInit} from '@angular/core';
import {LayoutProviderService} from '../layout-provider.service';
import {FooterMenuItem} from '../../common/class/layout-item';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit, OnDestroy {
  visible: boolean;
  menus: FooterMenuItem[];

  constructor(public layoutService: LayoutProviderService) {
    this.visible = this.layoutService.getFooters().visible;
    this.menus = this.layoutService.getFooters().menus;

    this.layoutService.layoutFooterSubject.subscribe((footer)=>{
      this.visible = footer.visible;
      this.menus = footer.menus;
    })
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.layoutService.layoutFooterSubject.unsubscribe()
  }

}
