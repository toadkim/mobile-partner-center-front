import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {
  @Input() svgIconName: string;
  @Input() label: string
  @Input() routerLink: string;

  constructor() {
    this.svgIconName = '';
    this.label = '';
    this.routerLink = '';
  }

  ngOnInit(): void {
  }

}
