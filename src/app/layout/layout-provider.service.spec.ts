import { TestBed } from '@angular/core/testing';

import { LayoutProviderService } from './layout-provider.service';

describe('LayoutProviderService', () => {
  let service: LayoutProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LayoutProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
