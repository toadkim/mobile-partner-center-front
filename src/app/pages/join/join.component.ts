import {Component, OnInit} from '@angular/core';
import {LayoutProviderService} from '../../layout/layout-provider.service';
import {Enums, HeaderType, TosTemplateClassName} from '../../common/enum/enums';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from '../../common/class/custom-validators';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {CategoryService} from '../../common/services/category.service';
import {ApiResponse} from '../../common/class/api-response';
import {UniqueBusinessAsyncValidator, UniqueEmailAsyncValidator, UserService} from '../../common/services/user.service';

export class Category {
  id: number;
  name: string;
  parentId: number;
  parentName: string;
  groupCode: string;
  divisionCode: string;

  //notifyInfoCode: string;
  constructor(id?: number, name?: string, parentId?: number, parentName?: string, groupCode?: string, divisionCode?: string) {
    this.id = id ? id : -1;
    this.name = name === undefined ? '' : name;
    this.parentId = parentId ? parentId : -1;
    this.parentName = parentName === undefined ? '' : parentName;
    this.groupCode = groupCode === undefined ? '' : groupCode;
    this.divisionCode = divisionCode === undefined ? '' : divisionCode;
  }

  get categoryName(): string {
    return [this.parentName, this.name].join(' > ');
  }
}

export interface TermsOfService {
  title: string;
  templateClassName: TosTemplateClassName,
  agree: boolean;
}

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.scss']
})
export class JoinComponent implements OnInit {

  userFormGroup: FormGroup = new FormGroup({
    email: new FormControl('',  {validators: [Validators.required, Validators.email], asyncValidators: [UniqueEmailAsyncValidator(this.userService)]}),
    passwordGroup: new FormGroup({
      password: new FormControl('', [Validators.required, Validators.pattern("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d~!@#$%^&*()+|=]{8,}$")]),
      passwordConfirm: new FormControl('', [Validators.required, CustomValidators.passwordConfirmValidator])
    }),
    tradersRegistration: new FormControl('', {validators:[Validators.required, Validators.pattern(/^[0-9]*$/)], asyncValidators: [UniqueBusinessAsyncValidator(this.userService)]}),
    representativeName: new FormControl('', [Validators.required]),
    businessName: new FormControl('', [Validators.required])
  }, []);
  categoryForm: FormControl = new FormControl('');
  fileUploaded: boolean = false;
  categories: Category[] = [];

  filteredOptions$: Observable<Category[]>;
  tosArray: TermsOfService[] = [
    {title: '상품 노출 약관 동의', templateClassName: TosTemplateClassName.ProductExposure, agree: false},
    {title: '상품 판매승인 약관 동의', templateClassName: TosTemplateClassName.ProductSalesApproval, agree: false},
    {title: '소비자 보호 관련 약관 동의', templateClassName: TosTemplateClassName.ConsumerProtection, agree: false},
    {title: '연령(만 14세 이상) 확인', templateClassName: TosTemplateClassName.AgeCheck, agree: false}
  ];


  constructor(private layoutService: LayoutProviderService, private categoryService: CategoryService, private userService: UserService) {
    this.layoutService.updateLayoutByPageType(HeaderType.OnlyArrow, Enums.NoFooter, '판매자 가입하기');
    console.log(this.categoryService.all().subscribe((response: ApiResponse) => {
      this.categories = response.data.categoryInfo.map(function (e: any) {
        return new Category(e.id, e.name, e.parentId, e.parentName, e.groupCode, e.divisionCode);
      });
    }, (error) => {
      console.error(error);
    }));

    this.filteredOptions$ = this.categoryForm.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      filter((name) => !!name),
      map(name => this._filter(name))
    );
  }

  ngOnInit(): void {
  }

  agreeCheck(agree: boolean, index: number) {
    this.tosArray[index].agree = true;
  }

  fileUploadCheck(control: FormControl) {
    control.valid ? this.fileUploaded = true : false;
  }

  private _filter(name: string): Category[] {
    return this.categories.filter(option => option.categoryName.includes(name));
  }


  get email(): AbstractControl | null {
    return this.userFormGroup.get('email');
  }

  get password(): AbstractControl | null {
    const passwordGroup = this.userFormGroup.get('passwordGroup');
    if (passwordGroup) {
      return passwordGroup.get('password')
    }
    return null;
  }

  get passwordConfirm(): AbstractControl | null {
    const passwordGroup = this.userFormGroup.get('passwordGroup');
    if (passwordGroup) {
      return passwordGroup.get('passwordConfirm')
    }
    return null;
  }

  get tradersRegistration(): AbstractControl | null {
    return this.userFormGroup.get('tradersRegistration');
  }

  get representativeName(): AbstractControl | null {
    return this.userFormGroup.get('representativeName');
  }

  get businessName(): AbstractControl | null {
    return this.userFormGroup.get('businessName');
  }

  get isTosAllChecked(): boolean {
    return this.tosArray.every((e) => {
      return e.agree == true;
    });
  }
}
