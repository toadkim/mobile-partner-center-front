import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TosTemplateClassName} from '../../../common/enum/enums';


@Component({
  selector: 'app-terms-of-service',
  templateUrl: './terms-of-service.component.html',
  styleUrls: ['./terms-of-service.component.scss']
})
export class TermsOfServiceComponent implements OnInit {

  @Input() title: string;
  @Input() templateClassName!: TosTemplateClassName;
  @Output() agreeEvent = new EventEmitter<boolean>();
  dialogRef?: MatDialogRef<any>;

  constructor(public dialog: MatDialog) {
    this.title = '';
  }

  ngOnInit(): void {
  }

  openDialog(name: TosTemplateClassName) {

    switch (name) {
      case TosTemplateClassName.ProductExposure:
        this.dialogRef = this.dialog.open(ToSProductExposureComponent)
        break;
      case TosTemplateClassName.ProductSalesApproval:
        this.dialogRef = this.dialog.open(ToSProductSalesApprovalComponent)
        break;
      case TosTemplateClassName.ConsumerProtection:
        this.dialogRef = this.dialog.open(ToSConsumerProtectionComponent)
        break;
      case TosTemplateClassName.AgeCheck:
        this.dialogRef = this.dialog.open(ToSAgeCheckComponent)
        break;
    }

    this.dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.agreeEvent.emit(result);
    });
  }


}

@Component({
  templateUrl: './content/product-exposure.html'
})
export class ToSProductExposureComponent implements OnInit {
  ngOnInit(): void {
  }
}

@Component({
  templateUrl: './content/product-sales-approval.html'
})
export class ToSProductSalesApprovalComponent implements OnInit {
  ngOnInit(): void {
  }
}


@Component({
  templateUrl: './content/consumer-protection.html'
})
export class ToSConsumerProtectionComponent implements OnInit {
  ngOnInit(): void {
  }
}


@Component({
  templateUrl: './content/age-check.html'
})
export class ToSAgeCheckComponent implements OnInit {
  ngOnInit(): void {
  }
}
