import {Component, OnInit} from '@angular/core';
import {LayoutProviderService} from '../../../layout/layout-provider.service';
import {HeaderType, Enums} from '../../../common/enum/enums';


@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor(private layoutService: LayoutProviderService) {
    this.layoutService.updateLayoutByPageType(HeaderType.OnlyArrow, Enums.NoFooter, '404 Not Found');
  }

  ngOnInit(): void {
  }

}
