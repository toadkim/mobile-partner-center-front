import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CampaignRoutingModule} from './campaign-routing.module';
import {CampaignComponent} from './campaign.component';
import {CommonComponentsModule} from '../../common/components/common-components.module';


@NgModule({
  declarations: [
    CampaignComponent
  ],
  imports: [
    CommonModule,
    CampaignRoutingModule,
    CommonComponentsModule
  ]
})
export class CampaignModule {
}
