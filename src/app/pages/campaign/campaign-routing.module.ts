import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CampaignComponent} from './campaign.component';
import {AuthenticationGuard} from '../../common/guards/authentication-guard.service';


const routes: Routes = [
  {
    path: 'campaign',
    loadChildren: () => import('./campaign.module').then(m => m.CampaignModule),
    component: CampaignComponent,
    canLoad: [AuthenticationGuard]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignRoutingModule {
}
