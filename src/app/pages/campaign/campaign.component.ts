import {Component, OnInit} from '@angular/core';
import {LayoutProviderService} from '../../layout/layout-provider.service';
import {HeaderType, Enums} from '../../common/enum/enums';


@Component({
  selector: 'app-campaigns',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {

  constructor(private layoutService: LayoutProviderService) {
    this.layoutService.updateLayoutByPageType(HeaderType.OnlyArrow, Enums.NoFooter, 'Campaign');
  }

  ngOnInit(): void {
  }

}
