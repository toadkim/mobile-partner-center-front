import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {LayoutProviderService} from '../../layout/layout-provider.service';
import {HeaderType, Enums} from '../../common/enum/enums';
import {AuthenticationService} from '../../common/services/authentication.service';
import {User} from '../../common/class/user';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email:FormControl;
  password: FormControl;

  constructor(private layoutService:LayoutProviderService, private authService: AuthenticationService, private router: Router) {
    this.layoutService.updateLayoutByPageType(HeaderType.None, Enums.NoHeaderAndNoFooter);
    this.email  = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(6)])
  }

  ngOnInit(): void {

  }

  loginProcess() {
    const newUser = new User();
    newUser.isLoggedIn = true;
    this.authService.signIn(newUser);
  }


  getErrorMessage() {
    if (this.email.hasError('required')) {
      return '이메일을 입력해 주세요.';
    }

    return this.email.hasError('email') ? '이메일 주소 형식이 올바르지 않습니다.' : '';
  }

  getErrorPasswordMessage() {
    if (this.password.hasError('required')) {
      return '비밀번호를 입력해 주세요.';
    }
    return this.password.hasError('minlength') ? '최소 6자 이상 입력해 주세요.' : '';
  }

}

