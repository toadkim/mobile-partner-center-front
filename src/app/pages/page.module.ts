import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {JoinComponent} from './join/join.component';
import {PageNotFoundComponent} from './error/page-not-found/page-not-found.component';

import {CampaignModule} from './campaign/campaign.module';
import {ShareModule} from '../common/modules/share.module';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {
  TermsOfServiceComponent, ToSAgeCheckComponent, ToSConsumerProtectionComponent,
  ToSProductExposureComponent,
  ToSProductSalesApprovalComponent
} from './join/terms-of-service/terms-of-service.component';
import {CommonComponentsModule} from '../common/components/common-components.module';


@NgModule({
  declarations: [
    LoginComponent,
    JoinComponent,
    PageNotFoundComponent,
    TermsOfServiceComponent,
    ToSProductExposureComponent,
    ToSProductSalesApprovalComponent,
    ToSConsumerProtectionComponent,
    ToSAgeCheckComponent
  ],
  imports: [
    CommonModule,
    ShareModule,
    CampaignModule,
    ReactiveFormsModule,
    RouterModule,
    CommonComponentsModule
  ]
})
export class PageModule {
}
